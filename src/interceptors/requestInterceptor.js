import axios from 'axios';

axios.interceptors.request.use(
  request => {
    request.url+=`&appid=${process.env.REACT_APP_WEATHER_API_KEY}`;
    return request;
  }, 
  error => Promise.reject(error)
)