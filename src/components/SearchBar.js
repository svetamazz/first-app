import React from 'react';
import debounce from 'lodash/debounce';
import {ajaxWeatherCall} from '../services/axios-service';
import {useWeather} from '../contexts/weatherContext';
import TextField from '@material-ui/core/TextField';

function SearchBar() {

  const [state, dispatch] = useWeather();

  const handleSearchTextChange = (searchText) => {
    ajaxWeatherCall(searchText)
    .then(response => dispatch({type:'GET_WEATHER',payload:response}))
    .catch(err => dispatch({type:'GET_WEATHER',payload:err.response}));
  }

  const delayedHandleChange = debounce((val)=>{handleSearchTextChange(val)}, 1000);

  const handleChange = (e) => delayedHandleChange(e.target.value);

  return (<TextField label="Enter city" onChange={handleChange} variant="outlined" />);
}

export default SearchBar;