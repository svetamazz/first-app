import React from 'react';
import WeatherCard from './WeatherCard';
import SearchBar from './SearchBar';
import {WeatherProvider} from '../contexts/weatherContext';
import AboutWeather from './AboutWeather';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch
} from "react-router-dom";

function WeatherPanel() {

  let { path, url } = useRouteMatch();
  
  return (
    <>
    <Router>
      <WeatherProvider>
        <SearchBar/>
        <WeatherCard/>
      </WeatherProvider>

      <Link to={`${url}/about`}> About </Link>

      <Switch>
        <Route exact path={`${path}/about`} component={AboutWeather}/>
      </Switch>
    </Router>
    </>
  );
}

export default WeatherPanel;