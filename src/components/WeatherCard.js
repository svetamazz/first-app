import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { useWeather } from '../contexts/weatherContext';

const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
      margin: '20px 0',
      paddingLeft: '5px',
      boxShadow: '0px 0.5px 5px 0px rgba(0,0,0,0.75)',
      background: '#d9a7c7',
      background: 'linear-gradient(to right, #fc5c7de3, #6a82fbc9)',
      color: 'white'
    },
    details: {
      display: 'flex',
      flexDirection: 'column',
    },
    content: {
      flex: '1 0 auto',
    },
    cover: {
      width: '8.3rem',
      borderRadius:'47%',
      margin: '-20px -5px 3px 0',
      backgroundColor: '#ffffff40;'
    },
}));

function WeatherCard () {
  const [state,dispatch] = useWeather();

  const classes = useStyles();
  const response = state;

  let output=(<p></p>);

  if(response){
      if(response.status === 200){
          const weather = response.data.weather[0];
          const imgURL=`http://openweathermap.org/img/wn/${weather.icon}@2x.png`;
          const main = response.data.main;
          const date = (new Date()).toLocaleString('default', { month: 'long',day:'numeric',year:'numeric' });
          
          output = (
            <Card className={classes.root}>
              <div className={classes.details}>
                <CardContent className={classes.content}>
                  <Typography component="h5" variant="h5"> {response.data.name} </Typography>
                  <Typography variant="body2"> {date} </Typography>
                  <Typography component="h3" variant="h3"> {parseInt(main.temp)}&deg; </Typography>
                  <Typography component="p" variant="p"> <b>{parseInt(main.temp_max)} &deg;</b>/ <b>{parseInt(main.temp_min)} &deg;</b> </Typography>
                </CardContent>
              </div>
              <div className={classes.details} align="center">
              <img className={classes.cover} src={imgURL} />
                <Typography component="h5" variant="h5"> {weather.main} </Typography>
              </div>
            </Card>
          );
      }
      else output = (<p style={{color: "red"}}>Error: {response.data.message}</p>);
    }
    return output;
  };

  export default WeatherCard;