import React, { useReducer, useContext} from 'react';
import weatherReducer from '../reducers/weatherReducer';

export const WeatherContext = React.createContext();

export const WeatherProvider = ({ children }) => {
  const contextValue = useReducer(weatherReducer, null);
  return (
    <WeatherContext.Provider value={contextValue}>
      {children}
    </WeatherContext.Provider>
  );
};

export const useWeather = () => {
    const contextValue = useContext(WeatherContext);
    return contextValue;
};