import axios from 'axios';

const weatherURL='http://api.openweathermap.org/data/2.5/weather?units=metric&q=';
export const ajaxWeatherCall = (search) => {return axios.get(weatherURL+search)};
