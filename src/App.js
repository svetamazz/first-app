import React from 'react';
import './App.css';
import 'typeface-roboto';
import './interceptors/requestInterceptor';
import WeatherPanel from './components/WeatherPanel';
import Home from './components/Home';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import ROUTING from './routing/routing';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const useStyles = makeStyles(theme => ({
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    flexWrap: 'wrap',
  },
  headerTitle: {
    flexGrow: 1,
    color:'white',
    border:'none',
    textDecoration:'none',
  },
  headerLink: {
    margin: theme.spacing(1, 1.5),
    color:'white',
    border:'none',
    textDecoration:'none',
  },
}));

function App() {

  const {
    HOME,
    WEATHER,
    ABOUT
  } = ROUTING;

  const classes = useStyles();

  return (
    <>
    <Router>

      <AppBar position="static" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Link to={HOME} className={classes.headerTitle}>
            <Typography variant="h6" noWrap> WeatherApp </Typography>
          </Link>  
          <Link to={WEATHER} className={classes.headerLink}> Weather </Link>
          {/* <Link to={ABOUT} className={classes.headerLink}> About </Link> */}
        </Toolbar>
      </AppBar>

      <div className="content">
        <Switch>
            <Route exact path={HOME} component={Home}/>
            <Route exact path={WEATHER} component={WeatherPanel}/>
        </Switch>
      </div>

    </Router>  
    </>
  );
}

export default App;